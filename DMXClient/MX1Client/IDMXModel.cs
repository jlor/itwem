﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsPhoneTest
{
    interface IDMXModel
    {
        GoboEnum GoboStatus { get; set; }
        ShutterEnum ShutterStatus { get; set; }
        byte HorizontalPosition{get;set;}
        byte VerticalPosition { get; set; }
        byte GoboSpeed { get; set; }
        byte DimmerIntensity { get; set; }

        byte[] DMXPacket { get; }
    }
}
