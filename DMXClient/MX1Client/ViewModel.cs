﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Data;
using System.Diagnostics;
using Microsoft.Phone.Shell;

namespace WindowsPhoneTest
{
    public class ViewModel : INotifyPropertyChanged
    {
        private IDMXModel model;
        private INetworkClient streamingACNClient;

        public ViewModel()
        {
            model = MartinMX1Model.Model;
            streamingACNClient = new StreamingACNClient();
        }



        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                // Send out new DMX packets on changes.
                streamingACNClient.sendDMXPacket(model.DMXPacket);
            }
        }
        
        private Rect AtouchRectangle = new Rect();
        /// <summary>
        /// Representation of the touchable section of our navigation rectangle on the phone
        /// </summary>
        public Rect ATouchRectangle 
        {
            get
            {
                return AtouchRectangle;
            }
            set
            {
                AtouchRectangle = value;
            }
        }

        /// <summary>
        /// Horizontal coordinate for the navigation rectangle on the phone
        /// </summary>
        public double HorizontalPosition {
            get
            {
                return (double)((model.HorizontalPosition * ATouchRectangle.Width) / byte.MaxValue);
            }
            set
            {
                model.HorizontalPosition = (byte)((value / ATouchRectangle.Width) * byte.MaxValue);
                NotifyPropertyChanged("HorizontalPosition");
            }
        }

        /// <summary>
        /// Vertical coordinate for the navigation rectangle on the phone
        /// </summary>
        public double VerticalPosition
        {
            get
            {
                return (double)((model.VerticalPosition * ATouchRectangle.Height) / byte.MaxValue);
            }
            set
            {
                model.VerticalPosition = (byte)((value / ATouchRectangle.Height) * byte.MaxValue);
                NotifyPropertyChanged("VerticalPosition");
            }
        }

        /// <summary>
        /// String representing our current shutter status
        /// </summary>
        public string ShutterStatus 
        {
            get
            {
                return Enum.GetName(typeof(ShutterEnum), model.ShutterStatus);
            }
            set
            {
                if (Enum.IsDefined(typeof(ShutterEnum), value))
                {
                    ShutterEnum tempShutterStatus = (ShutterEnum)Enum.Parse(typeof(ShutterEnum), value, true);
                    if (model.ShutterStatus != tempShutterStatus)
                    {
                        model.ShutterStatus = tempShutterStatus;
                        NotifyPropertyChanged("ShutterStatus");
                    }
                }
            }
        }

        /// <summary>
        /// String representing our current Gobo status
        /// </summary>
        public string GoboStatus
        {
            get
            {
                return Enum.GetName(typeof(GoboEnum), model.GoboStatus);
            }
            set
            {
                if (Enum.IsDefined(typeof(GoboEnum), value))
                {
                    GoboEnum tempGoboStatus = (GoboEnum)Enum.Parse(typeof(GoboEnum), value, true);
                    if (model.GoboStatus != tempGoboStatus)
                    {
                        model.GoboStatus = tempGoboStatus;
                        NotifyPropertyChanged("GoboStatus");
                    }
                }
            }
        }

        /// <summary>
        /// Function to convert between 5-154 values (fast to slow) for tracking speed,
        /// to percentage for our slider (0-100), 100% being fast, 0% being slow.
        /// </summary>
        public double DimmerIntensity
        {
            get
            {
                // Conversion from byte (offset by 5) to percentage.
                return ((float)(model.DimmerIntensity - 5) / 154 ) * 100;
            }
            set
            {
                // Conversion from percentage to byte. Offset by 5.
                model.DimmerIntensity = (byte)((value / 100 * 154) + 5);
                NotifyPropertyChanged("DimmerIntensity");
            }
        }

    }
}
