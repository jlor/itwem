﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Unsupported;
using Phone.Controls;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Media;

namespace WindowsPhoneTest
{
    public partial class MainPage : PhoneApplicationPage
    {
        App application = (App)Application.Current;
        ViewModel vm = null;
        PickerBoxDialog colorPickerBoxDialog;
        PickerBoxDialog shutterPickerBoxDialog;

        public MainPage()
        {
            InitializeComponent();

            vm = application.viewModel;

            // Enable tilt effect in our Picker Box Dialog
            TiltEffect.SetIsTiltEnabled(this, true);
                        
            // PickerBoxDialog functionality provided by Phone.Controls project.
            // Credits to Alex Yakhnin, http://blogs.msdn.com/b/priozersk/archive/2010/09/12/implementing-picker-box-functionality-on-wp7.aspx
            InitShutterPickerBoxDialog();
            InitColorPickerBoxDialog();

            
            Touch.FrameReported += new TouchFrameEventHandler(Touch_FrameReported);
        }

        private bool canvasEnabled = true;
        private TouchPoint touchDown = null;

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            TouchPoint tp = e.GetPrimaryTouchPoint(PositionCanvas);

            // We dont want rectangles we can't see.
            if (vm.ATouchRectangle.Height == 0 ||
                vm.ATouchRectangle.Width == 0)
            {
                vm.ATouchRectangle = new Rect(Dot.ActualWidth / 2, Dot.ActualHeight / 2, PositionCanvas.ActualWidth - Dot.ActualWidth, PositionCanvas.ActualHeight - Dot.ActualHeight);
            }

            // Update our touched point every time we touch the screen
            if (tp.Action == TouchAction.Down)
            {
                touchDown = tp;
            }

            // Stop reacting after we release our finger from the screen
            if (tp.Action == TouchAction.Up)
            {
                touchDown = null;
            }

            // Make sure both the initial touched coordinate AND the current coordinate
            // reside inside our (visible) canvas.
            if (touchDown != null &&
                canvasEnabled &&
                vm.ATouchRectangle.Contains(touchDown.Position) &&
                vm.ATouchRectangle.Contains(tp.Position))
            {
                vm.HorizontalPosition = (tp.Position.X - vm.ATouchRectangle.Left);
                vm.VerticalPosition = (tp.Position.Y - vm.ATouchRectangle.Top);
            }
            
        }
        
        // Following 2 GetValues functions credit Einar Ingebrigtsen:
        // http://www.dolittle.com/blogs/einar/archive/2008/01/13/missing-enum-getvalues-when-doing-silverlight-for-instance.aspx
        public static T[] GetValues<T>()
        {
            Type enumType = typeof(T);

            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Type '" + enumType.Name + "' is not an enum");
            }

            List<T> values = new List<T>();

            var fields = from field in enumType.GetFields()
                         where field.IsLiteral
                         select field;

            foreach (FieldInfo field in fields)
            {
                object value = field.GetValue(enumType);
                values.Add((T)value);
            }

            return values.ToArray();
        }
        public static object[] GetValues(Type enumType)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Type '" + enumType.Name + "' is not an enum");
            }

            List<object> values = new List<object>();

            var fields = from field in enumType.GetFields()
                         where field.IsLiteral
                         select field;

            foreach (FieldInfo field in fields)
            {
                object value = field.GetValue(enumType);
                values.Add(value);
            }

            return values.ToArray();
        }

        private void InitShutterPickerBoxDialog()
        {
            shutterPickerBoxDialog = new PickerBoxDialog();

            shutterPickerBoxDialog.Title = "Set lamp status";
            shutterPickerBoxDialog.ItemSource = GetValues(typeof(ShutterEnum));
            shutterPickerBoxDialog.Closed +=new EventHandler(shutterPickerBoxDialog_Closed);
        }

        void shutterPickerBoxDialog_Closed(object sender, EventArgs e)
        {
            vm.ShutterStatus = Enum.GetName(typeof(ShutterEnum), shutterPickerBoxDialog.SelectedItem);
            // Re-enable canvas selection
            canvasEnabled = true;
        }

        private void InitColorPickerBoxDialog()
        {
            colorPickerBoxDialog = new PickerBoxDialog();

            colorPickerBoxDialog.Title = "Set gobowheel position";
            colorPickerBoxDialog.ItemSource = GetValues(typeof(GoboEnum));
            colorPickerBoxDialog.Closed += new EventHandler(colorPickerBoxDialog_Closed);
        }

        void colorPickerBoxDialog_Closed(object sender, EventArgs e)
        {
            vm.GoboStatus = Enum.GetName(typeof(GoboEnum), colorPickerBoxDialog.SelectedItem);
            // Re-enable canvas selection
            canvasEnabled = true;
        }

        private void button_lightCtl_Click(object sender, RoutedEventArgs e)
        {
            // Show dropdown for light control and disable canvas selection
            canvasEnabled = false;
            colorPickerBoxDialog.Show();
        }

        private void button_shutterCtl_Click(object sender, RoutedEventArgs e)
        {
            // Show dropdown for shutter control and disable canvas selection
            canvasEnabled = false;
            shutterPickerBoxDialog.Show();
        }
        
        private void Vertical_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                vm.VerticalPosition = e.NewValue;
            }
            catch { }
        }

        private void Horizontal_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                vm.HorizontalPosition = e.NewValue;
            }
            catch { }
        }

        private void Speed_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                // This works in percentages 0-100
                vm.DimmerIntensity = e.NewValue;
            }
            catch { }
        }

    }
}