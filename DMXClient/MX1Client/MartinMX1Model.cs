using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Collections.ObjectModel;
using System.Windows.Data;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;


namespace WindowsPhoneTest
{
    /// <summary>
    /// Three different states our shutter can have.
    /// </summary>
    public enum ShutterEnum
    {
        LightOff = 2,       // 0-4
        Open = 162,         // 155-169
        Strobe = 199,       // 170-229
        Reset = 252         // 250-255
    }

    /// <summary>
    /// The different colors we support.
    /// This can easily be extended by providing the remaining colors from p.23 in the MX-4 manual.
    /// </summary>
    public enum GoboEnum
    {
        Open = 6,           // 0-11
        Position2 = 18,     // 12-23
        Position3 = 30,     // 24-35
        Position4 = 42,     // 36-47
        Position5 = 54,     // 48-59
        Position6 = 66,     // 60-71
        Position7 = 78,     // 72-83
        Position8 = 90,     // 84-95
        Position9 = 102,    // 96-107
        Position10 = 114,   // 108-119
        Position11 = 126,   // 120-131
        Position12 = 138,   // 132-143
        Position13 = 150,   // 144-155
        Position14 = 162,   // 156-167
        Position15 = 174,   // 168-179
        Position16 = 186,   // 180-191
        Position17 = 198,   // 192-203
        Position18 = 210,   // 204-215
        Position19 = 222,   // 216-227
        Closed = 234        // 228-239
        // Split colors not supported yet.
    }

    public class MartinMX1Model : IDMXModel
    {
        private ShutterEnum currentShutterStatus = ShutterEnum.LightOff;    // Start with light off
        private GoboEnum currentGoboStatus = GoboEnum.Open;                 // Start with shutter open
        private byte currentVerticalPosition = 127;                         // Start vertical middle
        private byte currentHorizontalPosition = 127;                       // Start horizontal middle
        private byte currentDimmerIntensity = 154;                          // Start with quickest tracking speed
        private byte currentGoboSpeed = 3;                                  // Start with quickest gobo speed
        
        // Singleton implementation.
        private static MartinMX1Model model;

        /// <summary>
        /// Singleton class describing the DMX protocol for Martin MX-1
        /// </summary>
        public static MartinMX1Model Model
        {
            get
            {
                if (model == null)
                {
                    model = new MartinMX1Model();
                }
                return model;
            }
        }



        private MartinMX1Model()
        {
        }


        /// <summary>
        /// Set/Get the current gobo status as a Gobo Enum.
        /// </summary>
        public GoboEnum GoboStatus
        {
            get
            {
                return currentGoboStatus;
            }
            set
            {
                currentGoboStatus = value;
            }
        }

        /// <summary>
        /// Set/Get the current shutter status as a Shutter Enum.
        /// </summary>
        public ShutterEnum ShutterStatus
        {
            get
            {
                return currentShutterStatus;
            }
            set
            {
                currentShutterStatus = value;
            }
        }

        /// <summary>
        /// Byte representation of the vertical position of our light.
        /// 0-255: Up to down.
        /// 127: Neutral.
        /// </summary>
        public byte VerticalPosition
        {
            get
            {
                return currentVerticalPosition;
            }
            set
            {
                currentVerticalPosition = value;
            }
        }

        /// <summary>
        /// Byte representation of the horizontal position of our light.
        /// 0-255: Left to right.
        /// 127: Neutral.
        /// </summary>
        public byte HorizontalPosition
        {
            get
            {
                return currentHorizontalPosition;
            }
            set
            {
                currentHorizontalPosition = value;
            }
        }

        /// <summary>
        /// Speed of our light.
        /// 0-2: Tracking (speed off)
        /// 3-255: Fast to slow
        /// </summary>
        public byte DimmerIntensity
        {
            get
            {
                return currentDimmerIntensity;
            }
            set
            {
                currentDimmerIntensity = value;
            }
        }

        /// <summary>
        /// Speed of our color change.
        /// 0-255: Fast to slow
        /// </summary>
        public byte GoboSpeed
        {
            get
            {
                return currentGoboSpeed;
            }
            set
            {
                if (value != currentGoboSpeed)
                {
                    currentGoboSpeed = value;
                }
            }
        }

        public byte[] DMXPacket
        {
            get
            {
                byte[] buffer = new byte[7];

                // Channel 1: Light / Dimmer / Strobe / Stand-alone / Reset
                switch (ShutterStatus)
	            {
		            case ShutterEnum.LightOff:
                        buffer[0] = (byte)ShutterEnum.LightOff;
                        break;
                    case ShutterEnum.Open:
                        buffer[0] = DimmerIntensity;
                        break;
                    case ShutterEnum.Strobe:
                        buffer[0] = (byte)ShutterEnum.Strobe;
                        break;
                    case ShutterEnum.Reset:
                        buffer[0] = (byte)ShutterEnum.Reset;
                        break;
	            }

                // Channel 2: Not used
                // Channel 3: Color / Gobo Wheel
                buffer[2] = (byte)GoboStatus;

                // Channel 4: Pan
                buffer[3] = HorizontalPosition;

                // Channel 5: Tilt
                buffer[4] = VerticalPosition;

                // Channel 6: Pan/Tilt/Speed
                buffer[5] = 3;

                // Channel 7: Color/Gobo Speed
                //buffer[6] = colorGoboSpeed;

                return buffer;
            }
        }

    }
}
