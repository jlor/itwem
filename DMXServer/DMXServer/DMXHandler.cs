using System;
using Microsoft.SPOT;
using System.Threading;

namespace MAX3107SPITest
{
    public class DMXHandler
    {
        private readonly MAX3107 _max3107;

        private byte[] DMXbuffer = new byte[513];
        private byte[] transmitBuffer = new byte[33];
        private bool Breaking = true;

        private readonly Thread DMXSendThread;

        private int bufferPosition = 0;

        public DMXHandler(MAX3107 max3107)
        {
            _max3107 = max3107;

            /* Initially don't transmit */
            _max3107.TransmitDisabled = true;

            /*
             * 250 kbaud (actually 249925 baud)
             */
            _max3107.PLL = 96;

            _max3107.Divider = 14;
            _max3107.Fractional = 12;

            _max3107.PLLEnabled = true;
            
            _max3107.InternalOscillatorEnabled = true;

            /*
             * 8 data bits, 2 stop bits, no parity
             */
            _max3107.Rate = 1;
            _max3107.DataBits = 8;
            _max3107.StopBits = 2;
            _max3107.Parity = Parity.None;

            /* Empty buffer by resetting FIFO */
            _max3107.FIFOReset = true;
            _max3107.FIFOReset = false;

            /* Enable interrupts for future use */
            _max3107.IRQSel = true;

            /* Enable transmit */
            _max3107.TransmitDisabled = false;

            // This implements "START CODE (SC)"
            DMXbuffer[0] = 0x00;

            DMXSendThread = new Thread(DMXSend);
            DMXSendThread.Start();
        }

        // Control buffer and send data
        private void DMXSend()
        {
            while (true)
            {
                // Should we break? Have we emptied our buffer?
                if (Breaking &&
                    _max3107.TransmitFIFOLevel == 0)
                {
                    // Break for >= 88us and reset
                    _max3107.TransmitBreak = true;
                    Thread.Sleep(1);
                    _max3107.TransmitBreak = false;
                    
                    Breaking = false;
                }

                // We've reached the final position in the buffer.
                // Reset position and signal break is needed.
                if (bufferPosition == 513)
                {
                    bufferPosition = 0;

                    Breaking = true;
                }

                // We're not breaking and we've gone below our transmit threshold.
                // Copy new data in and write the buffer. Update position.
                if (!Breaking &&
                    _max3107.TransmitFIFOLevel < 95)
                {
                    int size = (bufferPosition == 0 ? 33 : 32);

                    Array.Copy(DMXbuffer, bufferPosition, transmitBuffer, 0, size);

                    _max3107.WriteData(transmitBuffer, size);

                    bufferPosition += size;
                }                
            }
        }

        /// <summary>
        /// Update our DMX buffer with supplied data.
        /// </summary>
        /// <param name="data">Require data to be 512 bytes maximum</param>
        public void UpdateDMXBuffer(byte[] data)
        {
            if (data.Length > DMXbuffer.Length - 1)
            {
                throw new ArgumentOutOfRangeException("data");
            }

            Array.Copy(data, 0, DMXbuffer, 1, data.Length);
        }
    }
}
