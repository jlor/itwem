using System;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Collections.ObjectModel;
using System.Windows.Data;
using System.ComponentModel;
using System.Windows.Threading;
using System.Text;
using System.Linq;


namespace WindowsPhoneTest
{
    public class StreamingACNClient : INetworkClient
    {
        private readonly static ushort Universe = 1;
        public readonly static int DiscoveryPort = 5568;
        public readonly static IPAddress DiscoveryAddress = new IPAddress(new byte[] { 239, 255, (byte)(Universe >> 8), (byte)Universe });

        
        UdpAnySourceMulticastClient client;
        bool joinedMulticastGroup = false;
        byte[] buffer;

        public StreamingACNClient()
        {
            // Join Multicast Group
            joinMulticastGroup();
        }

        private void joinMulticastGroup()
        {
            buffer = new byte[512];
            try
            {
                client = new UdpAnySourceMulticastClient(DiscoveryAddress, DiscoveryPort);

                client.BeginJoinGroup(
                    result =>
                    {
                        client.EndJoinGroup(result);
                        client.MulticastLoopback = false;
                        joinedMulticastGroup = true;
                    }, null);
            }
            catch { }
        }

        private void sendMulticastData(byte[] buffer)
        {
            if (joinedMulticastGroup)
            {
                // Generate our ACN packet
                byte[] data = GenerateRootLayer(buffer);

                client.BeginSendToGroup(data, 0, data.Length, result =>
                    {
                        client.EndSendToGroup(result);
                    }, null);
            }
            else
            {
                // Apparently we didn't join the group when we instantiated..
                joinMulticastGroup();
            }
        }

        /// <summary>
        /// Wrap up a DMX packet in an ACN stream and send it.
        /// </summary>
        /// <param name="DMXPacket">DMX packet to be sent via sACN</param>
        public void sendDMXPacket(byte[] DMXPacket)
        {
            sendMulticastData(DMXPacket);
        }


        // Generate Root Layer Protocol Packet
        private byte[] GenerateRootLayer(byte[] DMXvalues)
        {
            byte[] ACNPacketIdentifier = new byte[] { 0x41, 0x53, 0x43, 0x2D, 0x45,
                                                      0x31, 0x2E, 0x31, 0x37, 0x00,
                                                      0x00, 0x00 };
            Guid SenderUniqueID = new Guid("10095b6e-630e-2640-907a-6b2cb6e27d70");

            /* Framing Layer */
            var temp = GenerateFramingLayer(DMXvalues);

            byte[] result = new byte[temp.Length + 38];
            /* Preamble Size */
            Array.Copy(ConvertUshort(16), 0, result, 0, 2);

            /* Post-amble Size */
            Array.Copy(ConvertUshort(0), 0, result, 2, 2);

            /* ACN Packet Identifier */
            Array.Copy(ACNPacketIdentifier, 0, result, 4, 12);

            /* Flags and Length */
            Array.Copy(ProduceFlagsAndLength((ushort)(temp.Length + 22)), 0, result, 16, 2);

            /* Vector */
            Array.Copy(ConvertUint(4), 0, result, 18, 4);

            /* CID */
            Array.Copy(SenderUniqueID.ToByteArray(), 0, result, 22, 16);

            /* Streaming ACN */
            Array.Copy(temp, 0, result, 38, temp.Length);

            return result;
        }

        // Generate Framing Layer
        private byte[] GenerateFramingLayer(byte[] DMXvalues)
        {
            string SourceName = "Streaming ACN client";

            /* DMP layer */
            var temp = GenerateDMPLayer(DMXvalues);

            var result = new byte[temp.Length + 77];
            /* Flags and Length */
            Array.Copy(ProduceFlagsAndLength((ushort)result.Length), 0, result, 0, 2);

            /* Vector */
            Array.Copy(ConvertUint(2), 0, result, 2, 4);

            /* Source name */
            var sourceNameBytes = Encoding.UTF8.GetBytes(SourceName.ToCharArray(), 0, SourceName.Length);
            Array.Copy(sourceNameBytes, 0, result, 6, Math.Min(sourceNameBytes.Length, 63));

            /* Priority */
            result[70] = 100;

            /* Reserved */
            Array.Copy(ConvertUshort(0), 0, result, 71, 2);

            /* Sequence number */
            result[73] = 1;

            /* Options */
            result[74] = 0;

            /* Universe */
            Array.Copy(ConvertUshort(1), 0, result, 75, 2);

            /* DMP Layer */
            Array.Copy(temp, 0, result, 77, temp.Length);

            return result;
        }

        // Generate DMP Layer
        private byte[] GenerateDMPLayer(byte[] DMXvalues)
        {
            const byte DMXSTART = 0;

            byte[] result = new byte[DMXvalues.Length + 11];
            /* Flags and Length */
            Array.Copy(ProduceFlagsAndLength((ushort)result.Length), 0, result, 0, 2);

            /* Vector */
            result[2] = 0x02;

            /* Address Type & Data Type */
            result[3] = 0xA1;

            /* First Property Address */
            Array.Copy(ConvertUshort(0), 0, result, 4, 2);

            /* Address increment */
            Array.Copy(ConvertUshort(1), 0, result, 6, 2);

            /* Property value count */
            Array.Copy(ConvertUshort((ushort)(DMXvalues.Length + 1)), 0, result, 8, 2);

            /* DMX START code */
            result[10] = DMXSTART;

            /* Property values */
            Array.Copy(DMXvalues, 0, result, 11, DMXvalues.Length);

            return result;
        }

        private byte[] ProduceFlagsAndLength(ushort length)
        {
            const ushort Mask12Bit = 0x0FFF;
            const ushort Top4bits = 0x7000;

            return ConvertUshort((ushort)((length & Mask12Bit) | Top4bits));
        }

        private byte[] ConvertUshort(ushort value)
        {
            var temp = BitConverter.GetBytes(value);

            return BitConverter.IsLittleEndian ? temp.Reverse().ToArray() : temp;
        }

        private byte[] ConvertUint(uint value)
        {
            var temp = BitConverter.GetBytes(value);

            return BitConverter.IsLittleEndian ? temp.Reverse().ToArray() : temp;
        }

    }
}
