using System;
using Microsoft.SPOT;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace MAX3107SPITest
{
    public delegate void PacketReceivedHandler(byte[] data);

    public class StreamingACNServer
    {
        private const ushort Universe = 1;
        private readonly IPAddress s_localIP = new IPAddress(new byte[] { 192, 168, 2, 80 });
        private const int DiscoveryPort = 5568;
        private readonly IPAddress DiscoveryAddress = new IPAddress(new byte[] { 239, 255, (byte)(Universe >> 8), (byte)Universe });

        private readonly Thread receiveThread;
        private readonly Socket serverSocket;
        private byte[] receiveBuffer = new byte[1024];

        public StreamingACNServer()
        {
            receiveThread = new Thread(ReceiveMethod);
            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);           
        }

        /// <summary>
        /// Start the server
        /// </summary>
        public void Start()
        {
            IPEndPoint localEP = new IPEndPoint(s_localIP, DiscoveryPort);

            // Join Multicast Group
            byte[] discoveryAddr = DiscoveryAddress.GetAddressBytes();
            byte[] ipAddr = s_localIP.GetAddressBytes();
            byte[] multicastOpt = new byte[] { discoveryAddr[0], discoveryAddr[1], discoveryAddr[2], discoveryAddr[3],   // WsDiscovery Multicast Address: 239.255.255.250
                                               ipAddr       [0], ipAddr       [1], ipAddr       [2], ipAddr       [3] }; // Local IPAddress

            serverSocket.Bind(localEP);
            serverSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastInterface, ipAddr);
            serverSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, multicastOpt);

            receiveThread.Start();
        }

        // Receive thread
        private void ReceiveMethod()
        {
            while (true)
            {
                try
                {
                    Array.Clear(receiveBuffer, 0, receiveBuffer.Length);

                    EndPoint endpoint = new IPEndPoint(IPAddress.Any, 0);

                    int bytesReceived = serverSocket.ReceiveFrom(receiveBuffer, ref endpoint);

                    // Make sure the data received is > 125.
                    if (bytesReceived < 125) continue;

                    // Root - Preamble size, should be 0x0010 or die
                    if (ushortFromBytes(receiveBuffer, 0) != 0x0010) continue;

                    // Root - Post-amble size, should be 0x0000 or die
                    if (ushortFromBytes(receiveBuffer, 2) != 0x0000) continue;

                    // Root - ACN Packet Identifier
                    if (uintFromBytes(receiveBuffer, 4) != 0x4153432d ||
                        uintFromBytes(receiveBuffer, 8) != 0x45312e31 ||
                        uintFromBytes(receiveBuffer, 12) != 0x37000000)
                    {
                        continue;
                    }

                    // Root - Flags and Length
                    ushort Root_FlagsAndLength = ushortFromBytes(receiveBuffer, 16);
                    byte Root_Flags = (byte)((Root_FlagsAndLength & 0xF000) >> 12);
                    ushort Root_Length = (ushort)(Root_FlagsAndLength & 0x0FFF);
                    if (Root_Flags != 0x7) continue;

                    // Root - Vector
                    if (uintFromBytes(receiveBuffer, 18) != 0x00000004) continue;

                    // Framing - Flags and Length
                    ushort Framing_FlagsAndLength = ushortFromBytes(receiveBuffer, 38);
                    byte Framing_Flags = (byte)((Framing_FlagsAndLength & 0xF000) >> 12);
                    ushort Framing_Length = (ushort)(Framing_FlagsAndLength & 0x0FFF);
                    if (Framing_Flags != 0x7) continue;

                    // Framing - Vector
                    if (uintFromBytes(receiveBuffer, 40) != 0x00000002) continue;

                    // DMP - Flags and Length
                    ushort DMP_FlagsAndLength = ushortFromBytes(receiveBuffer, 115);
                    byte DMP_Flags = (byte)((DMP_FlagsAndLength & 0xF000) >> 12);
                    ushort DMP_Length = (ushort)(DMP_FlagsAndLength & 0x0FFF);
                    if (DMP_Flags != 0x7) continue;

                    // DMP - Vector
                    if (receiveBuffer[117] != 0x02) continue;

                    // DMP - Address Type & Data Type
                    if (receiveBuffer[118] != 0xa1) continue;

                    // DMP - First Property Address
                    if (ushortFromBytes(receiveBuffer, 119) != 0x0000) continue;

                    // DMP - Address Increment
                    if (ushortFromBytes(receiveBuffer, 121) != 0x0001) continue;

                    // DMP - Property Value Count
                    ushort DMP_PropertyValueCount = ushortFromBytes(receiveBuffer, 123);
                    if (DMP_PropertyValueCount == 0 ||
                        DMP_PropertyValueCount > 513 ||
                        DMP_PropertyValueCount > bytesReceived - 124)
                    {
                        continue;
                    }

                    byte[] propertyValues = new byte[DMP_PropertyValueCount];
                    // Copy DMX data part from sACN packet.
                    Array.Copy(receiveBuffer, 126, propertyValues, 0, DMP_PropertyValueCount);

                    // We have a good DMX packet -> raise packet received event!
                    if (OnPacketReceived != null) OnPacketReceived(propertyValues);
                }
                catch { }
            }
        }

        public event PacketReceivedHandler OnPacketReceived;

        private ushort ushortFromBytes(byte[] data, uint offset)
        {
            return (ushort)(data[offset + 0] << 8 | data[offset + 1]);
        }

        private uint uintFromBytes(byte[] data, uint offset)
        {
            return (uint)(data[offset + 0] << 24 | data[offset + 1] << 16 | data[offset + 2] << 8 | data[offset + 3]);
        }
    }
}
