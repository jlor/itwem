using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.Collections;
using System.Threading;

namespace MAX3107SPITest
{
    [Flags]
    public enum InterruptsEnabledEnum
    {
        /// <summary>
        /// Generate interrupt when the CTSInt interrupt bit is set.
        /// </summary>
        CTSIEn = 0x80,
        /// <summary>
        /// Receive-DMXbuffer is empty interrupt
        /// </summary>
        RxEmtyIEn = 0x40,
        /// <summary>
        /// Transmit-DMXbuffer is empty interrupt
        /// </summary>
        TxEmtyIEn = 0x20,
        /// <summary>
        /// Transmit-trigger interrupt
        /// </summary>
        TxTrgIEn = 0x10,
        /// <summary>
        /// Receiver-trigger interrupt
        /// </summary>            
        RxTrgIEn = 0x08,
        /// <summary>
        /// Status interrupt
        /// </summary>
        STSIEn = 0x04,
        /// <summary>
        /// Special-character interrupt
        /// </summary>
        SpclChrIEn = 0x02,
        /// <summary>
        /// Line-status error interrupt
        /// </summary>
        LSRErrIEn = 0x01,

        None = 0x00
    }

    public enum Parity
    {
        None,
        Even,
        Odd
    }

    [Flags]
    public enum InterruptStatus
    {
        CTSInt = 0x80,
        RxEmptyInt = 0x40,
        TxEmptyInt = 0x20,
        TFifoTrgInt = 0x10,
        RFifoTrgInt = 0x08,
        STSInt = 0x04,
        SpCharInt = 0x02,
        LSRErrInt = 0x01
    }

    internal enum Register
    {
        RHR = 0x00,
        THR = 0x00,
        IRQEn = 0x01,
        ISR = 0x02,
        MODE1 = 0x09,
        MODE2 = 0x0A,
        FIFOTrgLvl = 0x10,
        LCR = 0x0B,
        TxFIFOLvl = 0x11,
        //RxFIFOLvl = 0x12,
        PLLConfig = 0x1A,
        BRGConfig = 0x1B,
        DIVLSB = 0x1C,
        DIVMSB = 0x1D,
        CLKSource = 0x1E,
        //RevID = 0x1F
    }    

    public delegate void CTSHandler();
    public delegate void ReceiveBufferIsEmptyHandler();
    public delegate void TransmitBufferIsEmptyHandler();
    public delegate void ReceiveTriggerHandler();
    public delegate void TransmitTriggerHandler();
    public delegate void StatusChangeHandler();
    public delegate void SpecialCharacterHandler();
    public delegate void LineStatusErrorHandler();

    public class MAX3107
    {
        private const byte WriteBit = 0x80;
        private const byte ReadMask = 0x1F;

        private const bool SPI_ChipSelectActiveState = false; // Chip Select is held low while accessing
        private const uint SPI_ClockSelectSetupTime = 10;
        private const uint SPI_ClockSelectHoldTime = 10;
        private const bool SPI_ClockIdleState = false;
        private const bool SPI_ClockEdge = true; // Data is sampled on the rising edge
        private const uint SPI_ClockKHz = 10 * 1000;

        private readonly SPI spiDevice;
        private readonly InterruptPort IRQPort;

        //private const uint registerBufferSize = 2;
        //private byte[] readBuffer = new byte[registerBufferSize];
        //private byte[] writeBuffer = new byte[registerBufferSize];

        public event CTSHandler OnCTS;
        public event ReceiveBufferIsEmptyHandler OnReceiveBufferIsEmpty;
        public event TransmitBufferIsEmptyHandler OnTransmitBufferIsEmpty;
        public event TransmitTriggerHandler OnTransmitTrigger;
        public event ReceiveTriggerHandler OnReceiveTrigger;
        public event StatusChangeHandler OnStatusChanged;
        public event SpecialCharacterHandler OnSpecialCharacter;
        public event LineStatusErrorHandler OnLineStatusError;

        public MAX3107(SPI.SPI_module bus, Cpu.Pin RSTPin, Cpu.Pin CSPin, Cpu.Pin IRQPin)
        {
            OutputPort RST = new OutputPort(RSTPin, false);

            spiDevice = new SPI(
                new SPI.Configuration(CSPin,
                                      SPI_ChipSelectActiveState,
                                      SPI_ClockSelectSetupTime,
                                      SPI_ClockSelectHoldTime,
                                      SPI_ClockIdleState,
                                      SPI_ClockEdge,
                                      SPI_ClockKHz,
                                      bus)
            );
            
            Thread.Sleep(10);

            RST.Write(true);

            // Setup interrupts for future use.
            IRQPort = new InterruptPort(IRQPin, false, Port.ResistorMode.PullUp, Port.InterruptMode.InterruptEdgeLow);
            IRQPort.OnInterrupt += new NativeEventHandler(IRQPort_OnInterrupt);
            IRQPort.EnableInterrupt();            
        }

        private void IRQPort_OnInterrupt(uint data1, uint data2, DateTime time)
        {
            InterruptStatus ISR = (InterruptStatus)ReadRegister(Register.ISR);

            if ((ISR & InterruptStatus.CTSInt) == InterruptStatus.CTSInt && OnCTS != null) { OnCTS(); }
            if ((ISR & InterruptStatus.RxEmptyInt) == InterruptStatus.RxEmptyInt && OnReceiveBufferIsEmpty != null) { OnReceiveBufferIsEmpty(); }
            if ((ISR & InterruptStatus.TxEmptyInt) == InterruptStatus.TxEmptyInt && OnTransmitBufferIsEmpty != null) { OnTransmitBufferIsEmpty(); }
            if ((ISR & InterruptStatus.TFifoTrgInt) == InterruptStatus.TFifoTrgInt && OnTransmitTrigger != null) { OnTransmitTrigger(); }
            if ((ISR & InterruptStatus.RFifoTrgInt) == InterruptStatus.RFifoTrgInt && OnReceiveTrigger != null) { OnReceiveTrigger(); }
            if ((ISR & InterruptStatus.STSInt) == InterruptStatus.STSInt && OnStatusChanged != null) { OnStatusChanged(); }
            if ((ISR & InterruptStatus.SpCharInt) == InterruptStatus.SpCharInt && OnSpecialCharacter != null) { OnSpecialCharacter(); }
            if ((ISR & InterruptStatus.LSRErrInt) == InterruptStatus.LSRErrInt && OnLineStatusError != null) { OnLineStatusError(); }
        }

        // Helper function
        private byte ReadRegister(Register register)
        {
            byte[] output = new byte[] { (byte)((byte)register & ReadMask), 0 };
            byte[] input = new byte[2];

            spiDevice.WriteRead(output, input);

            return input[1];
        }

        // Helper function
        private void WriteRegister(Register register, byte value)
        {
            byte[] writeBuffer = new byte[2];
            byte[] readBuffer = new byte[2];

            writeBuffer[0] = (byte)(((byte)register & ReadMask) | WriteBit);
            writeBuffer[1] = value;

            spiDevice.WriteRead(writeBuffer, 0, 2, readBuffer, 0, 2, 0);
        }

        /// <summary>
        /// Get/set parity from the LCR (0x0B) register
        /// </summary>
        public Parity Parity
        {
            get
            {
                // bitmask 0x18: 00011000
                byte temp = (byte)((ReadRegister(Register.LCR) & 0x18) >> 3);

                // bitmask 0x01: 00000001
                if ((temp & 0x01) > 0)
                {
                    // bitmask 0x02: 00000010
                    return (temp & 0x02) > 0 ? MAX3107SPITest.Parity.Even : MAX3107SPITest.Parity.Odd;
                }
                else
                {
                    return MAX3107SPITest.Parity.None;
                }                
            }

            set
            {
                // Bitmask 0xE7: 11100111
                byte temp = (byte)(ReadRegister(Register.LCR) & 0xE7);

                switch (value)
                {
                    case Parity.None:
                        WriteRegister(Register.LCR, temp);
                        break;
                    case Parity.Even:
                        // Bitmask 0x18: 00011000
                        WriteRegister(Register.LCR, (byte)(temp | 0x18));
                        break;
                    case Parity.Odd:
                        // Bitmask 0x08: 00001000
                        WriteRegister(Register.LCR, (byte)(temp | 0x08));
                        break;
                }
            }
        }

        /// <summary>
        /// Get transmit FIFO level from the TxFIFOLvl (0x11) register.
        /// </summary>
        public byte TransmitFIFOLevel
        {
            get { return ReadRegister(Register.TxFIFOLvl); }
        }

        /// <summary>
        /// Write an amount of data to SPI if transmit FIFO level permits. 
        /// </summary>
        /// <param name="data">Data to write</param>
        /// <param name="size">Amount of data to send</param>
        public void WriteData(byte[] data, int size)
        {
            if (data.Length <= 128 &&
                data.Length < 128 - TransmitFIFOLevel)
            {
                byte[] buffer = new byte[size + 1];
                // Make sure to enable writing to the SPI.
                buffer[0] = (WriteBit | (byte)Register.THR);
                Array.Copy(data, 0, buffer, 1, size);

                spiDevice.Write(buffer);
            }
        }

        /// <summary>
        /// Added missing absolute function for double values.
        /// </summary>
        /// <param name="value">Double value to absolute</param>
        /// <returns>Absolute of double value</returns>
        private double Abs(double value)
        {
            if (value < 0)
            {
                return -value;
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Get/set Internal Oscillator enable state from the CLKSource (0x1E) register
        /// </summary>
        public bool InternalOscillatorEnabled
        {
            get
            {
                // Bitmask 0x01: 00000001
                return (ReadRegister(Register.CLKSource) & 0x01) > 0;
            }
            set
            {
                // Bitmask 0xFE: 11111110
                byte temp = (byte)(ReadRegister(Register.CLKSource) & 0xFE);
                temp |= (byte)(value ? 0x01 : 0);
                WriteRegister(Register.CLKSource, temp);
            }
        }

        /// <summary>
        /// Get/set Crystal enabled state from the CLKSource (0x1E) register
        /// </summary>
        public bool CrystalEnabled
        {
            get
            {
                // Bitmask 0x02: 00000010
                return (ReadRegister(Register.CLKSource) & 0x02) > 0;
            }
            set
            {
                // Bitmask 0xFD: 11111101
                byte temp = (byte)(ReadRegister(Register.CLKSource) & 0xFD);
                temp |= (byte)(value ? 0x02 : 0);
                WriteRegister(Register.CLKSource, temp);
            }
        }

        /// <summary>
        /// Get/set PLL Enabled status from the CLKSource (0x1E) register
        /// </summary>
        public bool PLLEnabled
        {
            get
            {
                // Bitmask 0x04: 00000100
                return (ReadRegister(Register.CLKSource) & 0x04) > 0;
            }
            set
            {
                // Bitmask 0xFB: 11111011
                byte temp = (byte)(ReadRegister(Register.CLKSource) & 0xFB);
                temp |= (byte)(value ? 0x04 : 0);
                // Bitmask 0xF7: 11110111
                // Bitmask 0xFF: 11111111
                temp &= (byte)(value ? 0xF7 : 0xFF); // PLLBypass bit
                WriteRegister(Register.CLKSource, temp);
            }
        }

        /// <summary>
        /// Get/set PLL Bypass status from the CLKSource (0x1E) register
        /// </summary>
        public bool PLLBypass
        {
            get
            {
                // Bitmask 0x08: 00001000
                return (ReadRegister(Register.CLKSource) & 0x08) > 0;
            }
            set
            {
                // Bitmask 0xF7: 11110111
                byte temp = (byte)(ReadRegister(Register.CLKSource) & 0xF7);
                temp |= (byte)(value ? 0x08 : 0);
                // Bitmask 0xFB: 11111011
                // Bitmask 0xFF: 11111111
                temp &= (byte)(value ? 0xFB : 0xFF); // PLLenable bit
                WriteRegister(Register.CLKSource, temp);
            }
        }

        /// <summary>
        /// Get/set External Clock from the CLKSource (0x1E) register
        /// </summary>
        public bool ExternalClock
        {
            get
            {
                // Bitmask 0x10: 00010000
                return (ReadRegister(Register.CLKSource) & 0x10) > 0;
            }
            set
            {
                // Bitmask 0xEF: 11101111
                byte temp = (byte)(ReadRegister(Register.CLKSource) & 0xEF);
                temp |= (byte)(value ? 0x10 : 0);
                WriteRegister(Register.CLKSource, temp);
            }
        }

        /// <summary>
        /// Get/set Transmit status from the MODE1 (0x09) register
        /// </summary>
        public bool TransmitDisabled
        {
            get
            {
                // Bitmask 0x02: 00000010
                return (ReadRegister(Register.MODE1) & 0x02) > 0;
            }
            set
            {
                // Bitmask 0xFD: 11111101
                byte temp = (byte)(ReadRegister(Register.MODE1) & 0xFD);
                temp |= (byte)(value ? 0x02 : 0);
                WriteRegister(Register.MODE1, temp);
            }
        }        

        /// <summary>
        /// Get/set PreDivider value from the PLLConfig (0x1A) register
        /// </summary>
        public byte PreDivider
        {
            get
            {
                // Bitmask 0x3F: 00111111
                return (byte)(ReadRegister(Register.PLLConfig) & 0x3F);
            }
            set
            {
                // Bitmask 0xC0: 11000000
                byte temp = (byte)(ReadRegister(Register.PLLConfig) & 0xC0);
                temp |= (byte)(value & 0x3F);
                WriteRegister(Register.PLLConfig, temp);
            }
        }        

        /// <summary>
        /// Get/set PLL value from the PLLConfig (0x1A) register
        /// </summary>
        public byte PLL
        {
            get
            {
                // Bitmask 0xC0: 11000000
                switch ((ReadRegister(Register.PLLConfig) & 0xC0) >> 6)
                {
                    case 1: return 48;
                    case 2: return 96;
                    case 3: return 144;
                    default: return 6;
                }
            }
            set
            {
                byte _PLL = 0;

                switch (value)
                {
                    case 48: _PLL = 1; break;
                    case 96: _PLL = 2; break;
                    case 144: _PLL = 3; break;
                    default: _PLL = 0; break;
                }
                // Bitmask 0x3F: 00111111
                byte temp = (byte)(ReadRegister(Register.PLLConfig) & 0x3F);
                temp |= (byte)(_PLL << 6);
                WriteRegister(Register.PLLConfig, temp);
            }
        }

        /// <summary>
        /// Get/set Fractional value from the BRGConfig (0x1B) register
        /// </summary>
        public byte Fractional
        {
            get
            {
                // Bitmask 0x0F: 00001111
                return (byte)(ReadRegister(Register.BRGConfig) & 0x0F);
            }
            set
            {
                // Bitmask 0xF0: 11110000
                byte temp = (byte)(ReadRegister(Register.BRGConfig) & 0xF0);
                temp |= (byte)(value & 0x0F);
                WriteRegister(Register.BRGConfig, temp);
            }
        }

        /// <summary>
        /// Get/set Divider value from the DIVMSB/DIVLSB (0x1C/0x1D) registers
        /// </summary>
        public ushort Divider
        {
            get
            {
                return (ushort)(ReadRegister(Register.DIVMSB) << 8 | ReadRegister(Register.DIVLSB));
            }
            set
            {
                var lsb = (byte)value;
                var msb = (byte)(value >> 8);

                WriteRegister(Register.DIVLSB, lsb);
                WriteRegister(Register.DIVMSB, msb);
            }
        }

        /// <summary>
        /// Get/set Rate value from the BRGConfig (0x1B) register
        /// </summary>
        public byte Rate
        {
            get
            {
                // Bitmask 0x30: 00110000
                var temp = (byte)((ReadRegister(Register.BRGConfig) & 0x30) >> 4);

                switch (temp)
                {
                    case 1: return 2;
                    case 2: return 4;
                    default: return 1;
                }
            }
            set
            {
                // Bitmask 0xCF: 11001111
                var temp = (byte)(ReadRegister(Register.BRGConfig) & 0xCF);
                var _rate = 0;

                switch (value)
                {
                    case 2: _rate = 1; break;
                    case 4: _rate = 2; break;
                    default: _rate = 0; break;
                }

                temp |= (byte)(_rate << 4);
                WriteRegister(Register.BRGConfig, temp);
            }
        }

        /// <summary>
        /// Get/set Data bits value from the LCR (0x0B) register
        /// </summary>
        public byte DataBits
        {
            get
            {
                // Bitmask 0x03: 00000011
                return (byte)((ReadRegister(Register.LCR) & 0x03) + 5);
            }
            set
            {
                // Bitmask 0xFC: 11111100
                byte temp = (byte)(ReadRegister(Register.LCR) & 0xFC);
                temp |= (byte)(System.Math.Max(5, System.Math.Min(8, value)) - 5);
                WriteRegister(Register.LCR, temp);               
            }
        }

        /// <summary>
        /// Get/set Stop Bits value from the LCR (0x0B) register
        /// </summary>
        public byte StopBits
        {
            get
            {
                // Bitmask 0x04: 00000100
                return (byte)(((ReadRegister(Register.LCR) & 0x04) >> 2) + 1);
            }
            set
            {
                // Bitmask 0xFB: 11111011
                byte temp = (byte)(ReadRegister(Register.LCR) & 0xFB);
                temp |= (byte)(((System.Math.Max(1, System.Math.Min(2, value))) - 1) << 2);
                WriteRegister(Register.LCR, temp);
            }
        }

        /// <summary>
        /// Get/set Transmit FIFO Trigger level from the FIFOTrgLvl (0x10) register
        /// </summary>
        public byte TransmitFIFOTrigger
        {
            get
            {
                // Bitmask 0x0F: 00001111
                byte temp = (byte)(ReadRegister(Register.FIFOTrgLvl) & 0x0F);
                return temp;
            }
            set
            {
                // Bitmask 0xF0: 11110000
                byte temp = (byte)(ReadRegister(Register.FIFOTrgLvl) & 0xF0);
                temp |= (byte)((value / 8) & 0x0F);
                WriteRegister(Register.FIFOTrgLvl, temp);
            }
        }

        /// <summary>
        /// Get the transmit buffer size.
        /// </summary>
        public byte TransmitBufferSize
        {
            get
            {
                return 128;
            }
        }

        /// <summary>
        /// Get/set Transmit Break status from the LCR (0x0B) register
        /// </summary>
        public bool TransmitBreak
        {
            get
            {
                // Bitmask 0x40: 00000100
                return (ReadRegister(Register.LCR) & 0x40) > 0;
            }
            set
            {
                // Bitmask 0xBF: 10111111
                byte temp = (byte)(ReadRegister(Register.LCR) & 0xBF);
                temp |= (byte)(value ? 0x40 : 0);
                WriteRegister(Register.LCR, temp);
            }
        }

        /// <summary>
        /// Get/set FIFO Reset status from the MODE2 (0x0A) register
        /// </summary>
        public bool FIFOReset
        {
            get
            {
                // Bitmask 0x02: 00000010
                return (ReadRegister(Register.MODE2) & 0x02) > 0;
            }
            set
            {
                // Bitmask 0xFD: 11111101
                byte temp = (byte)(ReadRegister(Register.MODE2) & 0xFD);
                temp |= (byte)(value ? 0x02 : 0);
                WriteRegister(Register.MODE2, temp);
            }
        }

        /// <summary>
        /// Get/set Interrupts Enabled value from the IRQEn (0x01) register
        /// </summary>
        public InterruptsEnabledEnum InterruptsEnabled
        {
            get
            {
                return (InterruptsEnabledEnum)ReadRegister(Register.IRQEn);
            }
            set
            {
                WriteRegister(Register.IRQEn, (byte)value);
            }
        }

        /// <summary>
        /// Get/set Receive Disabled status from the MODE1 (0x09) register
        /// </summary>
        public bool ReceiveDisabled
        {
            get
            {
                return (ReadRegister(Register.MODE1) & 0x01) > 0;
            }
            set
            {
                // Bitmask 0xFE: 11111110
                byte temp = (byte)(ReadRegister(Register.MODE1) & 0xFE);
                temp |= (byte)(value ? 0x01 : 0);
                WriteRegister(Register.MODE1, temp);
            }
        }

        /// <summary>
        /// Get/set IRQSel status from the MODE1 (0x09) register
        /// </summary>
        public bool IRQSel
        {
            get
            {
                // Bitmask 0x80: 10000000
                return (ReadRegister(Register.MODE1) & 0x80) > 0;
            }
            set
            {
                // Bitmask 0x7F: 01111111
                byte temp = (byte)(ReadRegister(Register.MODE1) & 0x7F);
                temp |= (byte)(value ? 0x80 : 0);
                WriteRegister(Register.MODE1, temp);
            }
        }        
    }
}
