﻿using System;

using Microsoft.SPOT;
using Microsoft.SPOT.Input;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Hardware;
using DeviceSolutions.SPOT.Hardware;
using System.IO.Ports;
using System.Threading;

namespace MAX3107SPITest
{
    enum Channel
    {
        Dimmer = 0,
        Gobo = 2,
        Pan = 3,
        Tilt = 4
    }

    public class Program : Microsoft.SPOT.Application
    {
        private readonly DMXHandler dmx;
        private readonly StreamingACNServer sACN;

        private byte horizontal = 127;
        private byte vertical = 127;
        private byte gobo = 0;
        private byte dimmer = 160;
        private byte speed = 10;

        private byte[] DMXpacket = new byte[6];

        Random random = new Random();

        public Program()
        {
            dmx = new DMXHandler(new MAX3107(SPI.SPI_module.SPI1, Meridian.Pins.GPIO1, Meridian.Pins.GPIO4, Meridian.Pins.GPIO11));
            sACN = new StreamingACNServer();
            sACN.OnPacketReceived += new PacketReceivedHandler(sACN_OnPacketReceived);
            sACN.Start();

            DMXpacket[(int)Channel.Dimmer] = dimmer;
            DMXpacket[(int)Channel.Gobo] = gobo;
            DMXpacket[(int)Channel.Pan] = horizontal;
            DMXpacket[(int)Channel.Tilt] = vertical;
            DMXpacket[5] = 3;
        }

        /// <summary>
        /// Handle new packets
        /// </summary>
        /// <param name="data">Data received</param>
        void sACN_OnPacketReceived(byte[] data)
        {
            dmx.UpdateDMXBuffer(data);
        }

        public static void Main()
        {
            Program myApplication = new Program();

            Window mainWindow = myApplication.CreateWindow();

            // Create the object that configures the GPIO pins to buttons.
            GPIOButtonInputProvider inputProvider = new GPIOButtonInputProvider(null);            

            // Start the application
            myApplication.Run(mainWindow);
        }             
        
        private Window mainWindow;

        /// <summary>
        /// Setup a simple line of text on the display.
        /// </summary>
        /// <returns></returns>
        public Window CreateWindow()
        {
            // Create a window object and set its size to the
            // size of the display.
            mainWindow = new Window();
            mainWindow.Height = SystemMetrics.ScreenHeight;
            mainWindow.Width = SystemMetrics.ScreenWidth;

            // Create a single text control.
            Text text = new Text();

            text.Font = Resources.GetFont(Resources.FontResources.small);
            text.TextContent = Resources.GetString(Resources.StringResources.String1);
            text.HorizontalAlignment = Microsoft.SPOT.Presentation.HorizontalAlignment.Center;
            text.VerticalAlignment = Microsoft.SPOT.Presentation.VerticalAlignment.Center;

            // Add the text control to the window.
            mainWindow.Child = text;

            // Connect the button handler to all of the buttons.
            mainWindow.AddHandler(Buttons.ButtonUpEvent, new RoutedEventHandler(OnButtonUp), false);

            // Set the window visibility to visible.
            mainWindow.Visibility = Visibility.Visible;

            // Attach the button focus to the window.
            Buttons.Focus(mainWindow);

            return mainWindow;
        }        

        private void OnButtonUp(object sender, RoutedEventArgs evt)
        {
            ButtonEventArgs e = (ButtonEventArgs)evt;
                        
            switch (e.Button)
            {
                case Button.VK_LEFT:
                    // Move scanner light cone to the left.
                    horizontal = (byte)System.Math.Max(0, horizontal - speed);
                    DMXpacket[3] = horizontal;
                    break;

                case Button.VK_RIGHT:
                    // Move scanner light cone to the right.
                    horizontal = (byte)System.Math.Min(255, horizontal + speed);
                    DMXpacket[3] = horizontal;
                    break;

                case Button.VK_UP:
                    // Move scanner light cone vertically up.
                    vertical = (byte)System.Math.Max(0, vertical - speed);
                    DMXpacket[4] = vertical;
                    break;

                case Button.VK_DOWN:
                    // Move scanner light cone vertically down.
                    vertical = (byte)System.Math.Min(255, vertical + speed);
                    DMXpacket[4] = vertical;
                    break;

                case Button.VK_SELECT:
                    // Cycle through the gobo wheel.
                    gobo = (byte)((gobo + 12) % 240);
                    DMXpacket[2] = gobo;
                    break;
            }

            dmx.UpdateDMXBuffer(DMXpacket);
        }
    }
}
